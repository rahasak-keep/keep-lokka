package com.score.lokka.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait KafkaConf {
  // config object
  val kafkaConf = ConfigFactory.load("kafka.conf")

  // kafka conf
  lazy val kafkaAddr = Try(kafkaConf.getString("kafka.addr")).getOrElse("dev.localhost:9092")
  lazy val kafkaGroup = Try(kafkaConf.getString("kafka.group")).getOrElse("lokkag")
  lazy val kafkaTopic = Try(kafkaConf.getString("kafka.topic")).getOrElse("lokka")
  lazy val kafkaRegistrarApiTopic = Try(kafkaConf.getString("kafka.registrarapi-topic")).getOrElse("registrarapi")
}
