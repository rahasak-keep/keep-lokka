package com.score.lokka.config

import com.typesafe.config.ConfigFactory

import scala.util.Try

trait SchemaConf {
  val schemaConf = ConfigFactory.load("schema.conf")

  // schemas
  lazy val schemaCreateKeyspace = Try(schemaConf.getString("schema.createKeyspace")).getOrElse("")
  lazy val schemaCreateTypeTrans = Try(schemaConf.getString("schema.createTypeTrans")).getOrElse("")
  lazy val schemaCreateTableTrans = Try(schemaConf.getString("schema.createTableTrans")).getOrElse("")
  lazy val schemaCreateTableBlocks = Try(schemaConf.getString("schema.createTableBlocks")).getOrElse("")
}