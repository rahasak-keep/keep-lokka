package com.score.lokka.util

import com.score.lokka.cassandra.{Block, Trans}
import com.score.lokka.config.AppConf

import scala.annotation.tailrec

object BlockFactory extends AppConf {

  def merkleRoot(trans: List[Trans]): String = {
    @tailrec
    def merkle(ins: List[String], outs: List[String]): String = {
      ins match {
        case Nil =>
          // empty list
          if (outs.size == 1) outs.head
          else merkle(outs, List())
        case x :: Nil =>
          // one element list
          merkle(Nil, outs :+ CryptoFactory.sha256(x + x))
        case x :: y :: l =>
          // have at least two elements in list
          // concat them and sign them
          merkle(l, outs :+ CryptoFactory.sha256(x + y))
      }
    }

    merkle(trans.map(t => CryptoFactory.sha256(t.id.toString)), List())
  }

  def hash(timestamp: String, markleRoot: String, preHash: String): String = {
    val p = timestamp + markleRoot + preHash
    CryptoFactory.sha256(p)
  }

  def block(trans: List[Trans]): Block = {
    Block(
      lokka = serviceName,
      merkelRoot = "",
      preHash = "",
      hash = "",
      trans = trans
    )
  }

}
